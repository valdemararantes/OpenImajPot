package neto.openimajpot

import org.openimaj.image.DisplayUtilities
import org.openimaj.image.ImageUtilities
import org.openimaj.image.MBFImage
import org.openimaj.image.colour.ColourSpace
import org.openimaj.image.colour.RGBColour
import org.openimaj.image.processing.convolution.FGaussianConvolve
import org.openimaj.image.typography.hershey.HersheyFont
import org.slf4j.LoggerFactory
import java.io.File
import java.net.URL
import javax.imageio.IIOException
import org.openimaj.image.processing.edges.CannyEdgeDetector
import com.hp.hpl.jena.vocabulary.RSS.image
import org.openimaj.ml.clustering.kmeans.FloatKMeans
import jdk.nashorn.tools.ShellFunctions.input
import com.twelvemonkeys.image.ImageUtil.getHeight
import org.openimaj.ml.clustering.FloatCentroidsResult
import java.util.*
import jdk.nashorn.tools.ShellFunctions.input
import com.hp.hpl.jena.sparql.pfunction.library.assign
import com.twelvemonkeys.image.ImageUtil.getHeight
import org.openimaj.ml.clustering.assignment.HardAssigner




/**
 * OpenIMAJ Hello world!
 *
 */
fun main(args: Array<String>) {
    with( App(), {
        //displayHelloWorld()
        //displayWebImage()
        clustering()
    })
}

class App {

    companion object {
        val log = LoggerFactory.getLogger(App::class.java)
    }

    fun clustering() {
        //val imgFile = "c:\\Users\\valdemar.neto\\Pictures\\face-mulher-fundo-branco-1.jpg"
        //val imgFile = "c:\\Users\\valdemar.neto\\Pictures\\Camera Roll\\valdemar_S7_20170307.jpg"
        val imgFile = "c:\\Users\\valdemar.neto\\Documents\\Certibio\\temp\\img_input_2017-09-14T14_20_17.821Z.png"
        try {
            var input = ImageUtilities.readMBF(File(imgFile))
            input = ColourSpace.convert(input, ColourSpace.CIE_Lab)
            //DisplayUtilities.display("Imagens", input, input1)

            val cluster = FloatKMeans.createExact(2)

            val imageData = input.getPixelVectorNative(Array(input.width * input.height) { FloatArray(3) })
            val result = cluster.cluster(imageData)
            val centroids = result.centroids
            for (fs in centroids) {
                log.info("centroids: ${Arrays.toString(fs)}")
            }

            val assigner = result.defaultHardAssigner()
            for (y in 0 until input.height) {
                for (x in 0 until input.width) {
                    val pixel = input.getPixelNative(x, y)
                    val centroid = assigner.assign(pixel)
                    input.setPixelNative(x, y, centroids[centroid])
                }
            }

            input = ColourSpace.convert(input, ColourSpace.RGB);
            DisplayUtilities.display(input);
        } catch (e: IIOException) {
            log.error("Erro ao acessar a URL $imgFile")
        }
    }

    fun displayWebImage() {
        log.info("Carregando imagem da WEB...")
        val image: MBFImage?
        //val imgUrl = "http://static.openimaj.org/media/tutorial/sinaface.jpg"
        val imgFile = "c:\\Users\\valdemar.neto\\Pictures\\face-mulher-fundo-branco-1.jpg"
        try {
            //image = ImageUtilities.readMBF(URL(imgUrl))
            image = ImageUtilities.readMBF(File(imgFile))
            log.info("image.colourSpace = ${(image.colourSpace)}")
            val processedImg = image.process(CannyEdgeDetector())
            DisplayUtilities.display("Imagens", image, processedImg)
        } catch (e: IIOException) {
            log.error("Erro ao acessar a URL $imgFile")
        }
    }

    fun displayHelloWorld() {
        //Create an image
        val image = MBFImage(320, 70, ColourSpace.RGB)

        //Fill the image with white
        image.fill(RGBColour.WHITE)

        //Render some test into the image
        image.drawText("Hello World", 10, 60, HersheyFont.CURSIVE, 50, RGBColour.BLACK)

        //Apply a Gaussian blur
        image.processInplace(FGaussianConvolve(2f))

        //Display the image
        DisplayUtilities.display(image)
    }

}
